
def test_astar():
    from astar.astar import a_star
    from astar.grid_path import GridPath

    walls  = {(0, 1), (1, 1), (-1, 1), (-1, 2)}
    dest = (0, 2)
    start = GridPath(dest=dest, walls=walls)
    a_star(start, dest).print()

def test_minmax():
    from minmax.tictactoe import TicTacToe
    from minmax.minmax import GameTree

    g = TicTacToe()
    g.move(1)
    while (g.get_result() is None):
        t = GameTree(g, 10)
        print("total size:", t.size())
        print("differents states:", len(t._closedSet))
        g = t.get_best_move()
        print(g)
    print(g.get_result())

print("test astar")
test_astar()
print("\ntest minmax")
test_minmax()
