from collections import Hashable

class GameState(Hashable, object):

    def __hash__(self):
        raise NotImplementedError()

    def __eq__(self, other):
        raise NotImplementedError()

    #def __gt__(self, other):
    #    raise NotImplementedError()

    def __str__(self):
        return "GameState"


    def possible_moves(self):
        raise NotImplementedError()

    def get_next_player(self):
        raise NotImplementedError()

    def get_result(self):
        raise NotImplementedError()

    def get_heuristic(self):
        raise NotImplementedError()

    def move_copy(self, move):
        raise NotImplementedError()
