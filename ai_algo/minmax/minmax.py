from minmax.game_state import GameState

class GameTree(object):

    def __init__(self, state, depth, closedSet = None):
        assert issubclass(type(state), GameState)
        self._closedSet = closedSet or {}
        #print(hash(state))
        #print(len(self._closedSet))
        self._state = state
        self._moves = []
        self._closedSet[state] = self
        if (depth > 0):
            for m in state.possible_moves():
                moved = state.move_copy(m)
                tree = self._closedSet.get(moved)
                if (tree is None):
                    self._moves.append(GameTree(moved,
                                                depth - 1,
                                                self._closedSet))
                else:
                    self._moves.append(tree)
        if (len(self._moves) > 0):
            if (state.get_next_player() == 1):
                self._bestMove = max(self._moves, key=lambda t: t._val)
            else:
                self._bestMove = min(self._moves, key=lambda t: t._val)
            self._val = self._bestMove._val
        else:
            self._val = state.get_heuristic()

    def get_best_move(self):
        return self._bestMove._state

    def size(self):
        return 1 + sum(map(GameTree.size, self._moves))
