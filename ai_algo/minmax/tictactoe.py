from minmax.game_state import GameState
from copy import deepcopy

class TicTacToe(GameState):

    # 0 1 2
    # 3 4 5
    # 6 7 8

    def __init__(self):
        self._player = 1
        self._game = [0] * 9
        self._result = None

    def __hash__(self):
        n = 0
        for p in self._game:
            n = n * 3 + p
        return n

    def __eq__(self, other):
        if (self._player == other._player
            and self._result == other._result
            and self._game == other._game):
            return True
        return False

    #def __gt__(self, other):
    #    raise NotImplementedError()

    def __str__(self):
        g = self._game
        return "\n".join(map(str, (g[:3], g[3:6], g[6:9])))

    _lines = ((0, 1, 2), (3, 4, 5), (6, 7, 8),
              (0, 3, 6), (1, 4, 7), (2, 5, 8),
              (0, 4, 8), (2, 4, 6))

    def _check_result(self, move):
        g = self._game
        for l in TicTacToe._lines:
            if (move in l):
                x, y, z = l
                if (0 != g[x] == g[y] == g[z]):
                    self._result = g[x]
                    break
        if (self._result is None
            and 0 not in g):
            self._result = 0

    def possible_moves(self):
        if (self._result is not None):
            return
        for i in range(9):
            if (self._game[i] == 0):
                yield i

    def get_next_player(self):
        return self._player

    def get_result(self):
        return self._result

    def get_heuristic(self):
        if (self._result is None
            or self._result == 0):
            return 0
        elif (self._result == 1):
            return 1
        return -1

    def move(self, move):
        self._game[move] = self._player
        self._player = 3 - self._player
        self._check_result(move)

    def move_copy(self, move):
        cp = deepcopy(self)
        cp.move(move)
        return cp
