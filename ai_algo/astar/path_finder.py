from collections import Hashable

class PathFinder(Hashable, object):

    def __hash__(self):
        raise NotImplementedError()

    def __eq__(self, other):
        raise NotImplementedError()

    def __gt__(self, other):
        raise NotImplementedError()


    def possible_moves(self):
        raise NotImplementedError()

    def state(self):
        raise NotImplementedError()

    def nb_moves(self):
        raise NotImplementedError()

    def is_dest(self):
        raise NotImplementedError()

    def move_copy(self, move):
        raise NotImplementedError()

    def print(self):
        raise NotImplementedError()
