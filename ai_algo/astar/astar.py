from astar.path_finder import PathFinder
from heapq import heappop, heappush

def a_star(start, heuristic=None, limit=9999):
    assert issubclass(type(start), PathFinder)
    openSet = [start]
    closedSet = {start}
    while (len(openSet) > 0 and limit > 0):
        limit -= 1
        position = heappop(openSet)
        print("current:", position)
        if (position.is_dest()):
            print("open set size:", len(openSet))
            print("closed set size:", len(closedSet))
            return position
        for move in position.possible_moves():
            newPos = position.move_copy(move)
            if (newPos not in closedSet):
                closedSet.add(newPos)
                heappush(openSet, newPos)
    return None
