from astar.path_finder import PathFinder

class GridPath(PathFinder):

    def __init__(self, position = None, dest = None, walls = None):
        self._position = position or (0, 0)
        self._dest = dest or (0, 0)
        self._walls = walls or set()
        self._dist = 0
        self._hDist = 0
        self._prev = None
        self.update_hdist()

    def __hash__(self):
        return hash(self._position)

    def __eq__(self, other):
        return self._position == other._position

    def __gt__(self, other):
        totSelf = self._dist + self._hDist
        totOther = other._dist + other._hDist
        if (totSelf == totOther):
            return self._hDist > other._hDist
        return totSelf > totOther

    def __str__(self):
        return " ".join(map(str, (self._dist, self._hDist, self._position)))


    def possible_moves(self):
        x1, y1 = self._position
        for x2, y2 in ((-1, 0), (1, 0), (0, -1), (0, 1)):
            p = x1 + x2, y1 + y2
            if (p not in self._walls):
                yield p
    
    def state(self):
        return self._position

    def nb_moves(self):
        return self._dist

    def is_dest(self):
        return self._position == self._dest

    def move_copy(self, move):
        new = GridPath(move, self._dest, self._walls)
        new._dist = self._dist + 1
        new._prev = self
        return new

    def update_hdist(self):
        (x1, y1), (x2, y2) = self._position, self._dest
        self._hDist = abs(x1 - x2) + abs(y1 - y2)
    
    def print(self):
        if (self._prev is not None):
            self._prev.print()
        print(self._position)
